const testData = [
	{
		test: '',
		result: 'ERROR'
	},
	{
		test: {
			commands: 'ABC'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0
			},
			commands: 'FRFRFRFR'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FRFRSDFGHJKLABC'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FFFFRRRR'
		},
		result: '{"x":4,"y":4}'
	},
	{
		test: {
			position: {
				x: 2,
				y: 2
			},
			commands: 'FRBL'
		},
		result: '{"x":2,"y":2}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FRFRFFFFFFFLLLLFFFFFRFFFFLFFLRRF'
		},
		result: '{"x":-1,"y":21}'
	},
	{
		test: {
			position: {
				x: 3,
				y: 6
			},
			commands: 'FFFFFFFFRRRRRRRFFFFLLLBBRRRRRLLLLLLLLLRFFF'
		},
		result: '{"x":4,"y":19}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 7
			},
			commands: 'RRRRRRRRFFFFFFFFFFFLLLBBBBBRRRLLLLLFFLR'
		},
		result: '{"x":3,"y":15}'
	}
];

export default testData;
