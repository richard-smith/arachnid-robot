const testDataMk2 = [
	{
		test: '',
		result: 'ERROR'
	},
	{
		test: {
			commands: 'ABC'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0
			},
			commands: 'FRFRFRFR'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FRFRSDFGHJKLABC'
		},
		result: 'ERROR'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FLF'
		},
		result: '{"x":0,"y":1}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FBFF'
		},
		result: '{"x":0,"y":0}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FRFBFF'
		},
		result: '{"x":0,"y":1}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FFRFFLFFBFF'
		},
		result: '{"x":2,"y":2}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 0
			},
			commands: 'FRFRFFFFFFFLLLLFFFFFRFFFFLFFLRRF'
		},
		result: '{"x":0,"y":0}'
	},
	{
		test: {
			position: {
				x: 3,
				y: 6
			},
			commands: 'FFFFFFFFRRRRRRRFFFFLLLBBRRRRRLLLLLLLLLRFFF'
		},
		result: '{"x":3,"y":14}'
	},
	{
		test: {
			position: {
				x: 0,
				y: 7
			},
			commands: 'RRRRRRRRFFFFFFFFFFFLLLBBBBBRRRLLLLLFFLR'
		},
		result: '{"x":2,"y":18}'
	}
];

export default testDataMk2;
