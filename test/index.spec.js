import { expect } from 'chai';

import { robotControl } from '../index';
import testData from './testData';
import testDataMk2 from './testDataMk2';

describe('robotControl test', () => {
	it('should execute Mk1 test data as expected', () => {
		testData.forEach(({ test, result }) => {
			const { position = '', commands = '' } = test;
			expect(JSON.stringify(robotControl(position, commands))).to.include(result);
		});
	});

	it('should execute Mk2 test data as expected', () => {
		testDataMk2.forEach(({ test, result }) => {
			const { position = '', commands = '' } = test;
			expect(JSON.stringify(robotControl(position, commands, 'mk2'))).to.include(result);
		});
	});
});
