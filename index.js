const COMMANDS_PATTERN = /^[FRBL]*$/gm;

const validateNegativePosition = ({ x, y }) => {
	return x >= 0 && y >= 0;
};

const robotControlMk1 = (position, commands) => {
	// Ascertain robot position by adding up number of moves in command directions
	const f = commands.filter((c) => c === 'F').length;
	const b = commands.filter((c) => c === 'B').length;
	const l = commands.filter((c) => c === 'L').length;
	const r = commands.filter((c) => c === 'R').length;

	// New x position is starting x plus right moves, minus left moves
	// New y position is starting y plus forward moves, minus backward moves
	return {
		x: parseInt(position.x) + r - l,
		y: parseInt(position.y) + f - b
	};
};

const robotControlMk2 = (position, commands) => {
	try {
		if (!validateNegativePosition(position)) {
			throw 'ERROR: robot initialised in negative space';
		}

		// Initialise position and orientation of robot
		let robot = {
			x: parseInt(position.x),
			y: parseInt(position.y),
			orientation: 'T'
		};

		// Iterate through all commands and reorientate and move the robot accordingly
		commands.forEach((command) => {
			switch (command) {
				case 'F':
					if (robot.orientation === 'T') {
						robot.y += 1;
					} else if (robot.orientation === 'R') {
						robot.x += 1;
					} else if (robot.orientation === 'B') {
						// prevent negative positions
						if (robot.y > 0) {
							robot.y -= 1;
						}
					} else {
						// prevent negative positions
						if (robot.x > 0) {
							robot.x -= 1;
						}
					}
					break;
				case 'R':
					if (robot.orientation === 'T') {
						robot.orientation = 'R';
					} else if (robot.orientation === 'R') {
						robot.orientation = 'B';
					} else if (robot.orientation === 'B') {
						robot.orientation = 'L';
					} else {
						robot.orientation = 'T';
					}
					break;
				case 'B':
					if (robot.orientation === 'T') {
						robot.orientation = 'B';
					} else if (robot.orientation === 'R') {
						robot.orientation = 'L';
					} else if (robot.orientation === 'B') {
						robot.orientation = 'T';
					} else {
						robot.orientation = 'R';
					}
					break;
				case 'L':
					if (robot.orientation === 'T') {
						robot.orientation = 'L';
					} else if (robot.orientation === 'R') {
						robot.orientation = 'T';
					} else if (robot.orientation === 'B') {
						robot.orientation = 'R';
					} else {
						robot.orientation = 'B';
					}
					break;
				default:
					break;
			}
		});

		return { x: robot.x, y: robot.y };
	} catch (error) {
		return error;
	}
};

// Main robotControl function - handles some param validation and then hands off to type specific functions
export const robotControl = (position, commands, type) => {
	try {
		if (!position || !Object.keys(position).includes('x') || !Object.keys(position).includes('y') || !commands) {
			throw 'ERROR: insufficient params provided';
		} else if (!commands.match(COMMANDS_PATTERN)) {
			throw 'ERROR: commands not recognised';
		}
		commands = commands.split('');

		if (type === 'mk2') {
			return robotControlMk2(position, commands);
		}

		return robotControlMk1(position, commands);
	} catch (error) {
		return error;
	}
};
